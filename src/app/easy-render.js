import EasyRenderer, { resultRenderer } from 'src/components/EasyRenderer'
import handleUpload from 'src/services/handleUpload'
import fields from './mock.json'

import './styles/easy-render.css'

if (localStorage.getItem('EasyRender')) {
  resultRenderer({
    fields,
    data: JSON.parse(localStorage.getItem('EasyRender')),
    target: document.querySelector('.main')
  })
} else {
  new EasyRenderer({
    fields,
    target: document.querySelector('.main'),
    onFormSubmit: data =>
      handleUpload(data)
      .then(parsedData => resultRenderer({
        fields,
        data: parsedData,
        target: document.querySelector('.main')
      }))
  })
}

