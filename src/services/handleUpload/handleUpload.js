const transformImageData = (data) => {
  const formData = data
  const filePromises = []

  Object.keys(formData).forEach((key) => {
    if (!(formData[key] instanceof File)) {
      return
    }

    filePromises.push(
      new Promise((resolve) => {
        const reader = new FileReader()
        reader.readAsDataURL(formData[key])
        reader.addEventListener('load', () => resolve({ key, value: reader.result }), false)
      })
    )
  })

  return Promise.all(filePromises)
  .then((files) => {
    files.forEach(({ key, value }) => (formData[key] = value))
    return formData
  })
}

const persistData = formData => new Promise(resolve =>
  setTimeout(() => {
    localStorage.setItem('EasyRender', JSON.stringify(formData))
    resolve(formData)
  }, 3000)
)

const handleUpload = data =>
  transformImageData(data)
  .then(persistData)

export default handleUpload
