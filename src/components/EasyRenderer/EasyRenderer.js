import InputField from './renderers/InputField'
import button from './renderers/button'
import map from './renderers/map'
import upload from './renderers/upload'
import image from './renderers/image'

import resultRenderer from './resultRenderer'

const getFormDataEntries = (formData) => {
  const obj = {}
  const iterator = formData.entries()
  let current = iterator.next()

  while (!current.done) {
    obj[current.value[0]] = current.value[1]
    current = iterator.next()
  }

  return obj
}

const isMaskValid = (mask, str) => {
  const exp = mask
              .replace(/(\.|-|\(|\))/g, match => `\\${match}`)
              .replace(/\s/g, () => '\\s')
              .replace(/9/g, () => '[0-9]')

  return new RegExp(exp).test(str)
}

const getFieldValue = (element) => {
  if (element.value) {
    return element.value
  }

  if (element.files && element.files[0]) {
    return element.files[0].name
  }

  return null
}

const validateField = ({ field, element }) => {
  const inputElement = document.querySelector(`#${field.id}`)
  const boundElement = field.bindTo && document.querySelector(`${field.bindTo}`)
  const value = getFieldValue(inputElement)

  element.classList.remove('-valid', '-error')

  if (field.required === false) {
    element.classList.add('-valid')
    return true
  }

  if (!value) {
    element.classList.add('-error')
    if (boundElement) {
      boundElement.classList.add('-error')
    }
    return false
  }

  if (field.mask && !isMaskValid(field.mask, value)) {
    element.classList.add('-error')
    return false
  }

  element.classList.add('-valid')

  if (boundElement) {
    boundElement.classList.add('-valid')
  }

  return true
}

class EasyRenderer {
  static RENDERER_STRATEGIES = {
    button,
    image,
    input: InputField,
    'map-renderer': map,
    'upload-button': upload
  };

  constructor({ fields, target, onFormSubmit }) {
    this.fields = fields
    this.target = target
    this.form = null
    this.renderedFields = []

    this._renderFields = this._renderFields.bind(this)
    this._onSubmit = this._onSubmit.bind(this)

    this._onFormSubmit = this._onFormSubmit.bind(this)
    this.onFormSubmit = onFormSubmit

    this.state = { isSubmitting: false }

    this._createForm()
  }

  _createForm() {
    const { target, _renderFields, _onSubmit } = this

    this.form = document.createElement('form')
    this.form.setAttribute('novalidate', 'novalidate')
    this.form.className = 'form'

    target.appendChild(this.form)

    _renderFields(this.form)

    this.form.addEventListener('submit', _onSubmit)
  }

  _renderFields(form) {
    const { fields } = this

    fields.forEach((field) => {
      const render = EasyRenderer.RENDERER_STRATEGIES[field.component] || null

      if (!render) {
        return
      }

      const element = render(field)
      form.appendChild(element)

      this.renderedFields.push({ field, element })
    })
  }

  _onSubmit(e) {
    e.preventDefault()

    if (this.state.isSubmitting) {
      return
    }

    const isValid = this.renderedFields
    .filter(({ field }) => field.name)
    .map(({ field, element }) => validateField({ field, element }))
    .reduce((currentValidity, fieldValidity) => // eslint-disable-line
      !currentValidity ? false : fieldValidity
    , true)

    if (!isValid) {
      return
    }

    this._setFormState({ isSubmitting: true })

    this._onFormSubmit()
    .then(() => this._setFormState({ isSubmitting: false }))
  }

  _onFormSubmit() {
    const formData = new FormData(this.form)

    return this.onFormSubmit(getFormDataEntries(formData))
  }

  _setFormState({ isSubmitting }) {
    const loadingModifier = '-loading'
    const submit = this.renderedFields.find(({ element }) => (element.type === 'submit'))
                   .element

    this.state = { ...this.state, isSubmitting }

    if (isSubmitting) {
      submit.classList.add(loadingModifier)
      this.form.classList.add(loadingModifier)
      return
    }
    submit.classList.remove(loadingModifier)
    this.form.classList.remove(loadingModifier)
  }
}

export default EasyRenderer

export { resultRenderer }
