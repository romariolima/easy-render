import headerLabel from './renderers/headerLabel'
import image from './renderers/image'
import button from './renderers/button'

const resultRenderer = ({ data, target, fields }) => {
  const frag = document.createDocumentFragment()

  fields.forEach(({ label, name, component }) => {
    if (!data[name]) {
      return
    }

    if (component !== 'upload-button') {
      frag.appendChild(
        headerLabel({
          label,
          content: data[name]
        })
      )
      return
    }

    if (component === 'upload-button') {
      frag.appendChild(
        image({
          id: name,
          src: data[name]
        })
      )
    }
  })

  const cleanButton = button({
    id: 'clean-button',
    name: '',
    label: 'Limpar Storage'
  })

  cleanButton.addEventListener('click', (e) => {
    e.preventDefault()
    localStorage.removeItem('EasyRender')
    location.reload()
  })

  frag.appendChild(cleanButton)

  while (target.lastChild) {
    target.removeChild(target.lastChild)
  }

  target.appendChild(frag)
}

export default resultRenderer
