import { expect } from 'chai'
import sinon from 'sinon'

import addons from './addons'

describe('Addons', () => {
  describe('#googleMapsAutoComplete()', () => {
    it('attach google maps auto complete to element', () => {
      /* global google */
      const element = { setAttribute: sinon.spy() }
      global.google = {
        maps: { places: { Autocomplete: sinon.spy() } }
      }

      addons.googleMapsAutoComplete(element)

      sinon.assert.calledWith(google.maps.places.Autocomplete, element, { types: ['geocode'] })
      sinon.assert.calledWith(element.setAttribute, 'placeholder', '')

      expect(element.setAttribute.calledAfter(google.maps.places.Autocomplete)).equal(true)

      delete global.google
    })
  })
})

