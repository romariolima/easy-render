/* global google */

const googleMapsAutoComplete = (element) => {
  /* eslint-disable no-new */
  new google.maps.places.Autocomplete(element, { types: ['geocode'] })
  element.setAttribute('placeholder', '')
}

export default { googleMapsAutoComplete }
