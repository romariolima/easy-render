import sinon from 'sinon'
import { expect } from 'chai'
import testUtils from 'test/utils'
import InputField from './InputField'

describe('Renderer - InputField', () => {
  it('render a form-field', () => {
    const inputFieldElement = InputField({
      id: 'id',
      type: 'text',
      name: 'input',
      value: 'value',
      label: 'Label'
    })
    const inputElement = inputFieldElement.children[0]
    const labelElement = inputFieldElement.children[1]

    expect(testUtils.getAllAttributes(inputFieldElement)).deep.equal({ class: 'form-field' })
    expect(inputFieldElement.nodeName).to.equal('DIV')

    expect(testUtils.getAllAttributes(inputElement)).deep.equal({
      class: 'form-field__input',
      id: 'id',
      name: 'input',
      type: 'text',
      required: 'required'
    })
    expect(inputElement.nodeName).equal('INPUT')
    expect(inputElement.value).equal('value')

    expect(testUtils.getAllAttributes(labelElement)).to.deep.equal({
      class: 'form-field__label',
      for: 'id'
    })
    expect(labelElement.nodeName).equal('LABEL')
    expect(labelElement.textContent).equal('Label')
  })

  it('input with mask', () => {
    const maskedInput = InputField.maskedInput
    const stub = sinon.stub().throws()
    const maskPattern = sinon.spy()

    stub.returns({ maskPattern })

    InputField.maskedInput = stub

    InputField({
      id: 'id',
      type: 'text',
      name: 'input',
      label: 'Label',
      mask: '(99) 99999-9999'
    })

    sinon.assert.calledWith(maskPattern, '(99) 99999-9999')

    InputField.maskedInput = maskedInput
  })

  it('suppot addon on input', () => {
    const availableAddons = InputField.availableAddons
    const addons = { foo: sinon.spy() }

    InputField.availableAddons = addons

    const formField = InputField({
      id: 'id',
      type: 'text',
      name: 'input',
      value: 'value',
      label: 'Label',
      addon: 'foo'
    })

    sinon.assert.calledWith(addons.foo, formField.querySelector('.form-field__input'))

    InputField.availableAddons = availableAddons
  })
})
