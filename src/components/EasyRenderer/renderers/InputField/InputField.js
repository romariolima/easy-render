import VMasker from 'vendor/vanilla-masker-1.1.0'
import fieldAddons from './addons'
import utils from '../utils'

const createWrap = () => {
  const wrapElement = document.createElement('div')
  wrapElement.setAttribute('class', 'form-field')

  return wrapElement
}

const createLabel = ({ htmlFor, label }) => {
  const element = document.createElement('label')
  const props = { class: 'form-field__label', for: htmlFor }

  utils.setAttributes(element, props)
  element.textContent = label

  return element
}

const createInputElement = ({
  id, type, name, value = '', mask, maskedInput, availableAddons, addon
}) => {
  const element = document.createElement('input')
  const props = { type, name, class: 'form-field__input', required: 'required' }

  utils.setAttributes(element, props)
  element.id = id
  element.value = value

  if (mask) {
    maskedInput(element).maskPattern(mask)
  }

  if (addon && availableAddons[addon]) {
    availableAddons[addon](element)
  }

  return element
}

const InputField = ({ name, id, label, value, type, mask, addon }) => {
  const { maskedInput, availableAddons } = InputField
  const wrapElement = createWrap()
  const labelElement = createLabel({ htmlFor: id, label })
  const inputElement = createInputElement({
    id, name, value, type, mask, maskedInput, availableAddons, addon
  })

  wrapElement.appendChild(inputElement)
  wrapElement.appendChild(labelElement)

  return wrapElement
}

InputField.maskedInput = VMasker
InputField.availableAddons = fieldAddons

export default InputField
