import utils from '../utils'

const button = ({ name, id, label, type = 'button', className = 'pill-button' }) => {
  const element = document.createElement('button')

  utils.setAttributes(element, { id, name, class: className, type })

  element.textContent = label

  return element
}

export default button
