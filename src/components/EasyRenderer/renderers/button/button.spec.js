import { expect } from 'chai'
import testUtils from 'test/utils'
import button from './button'

describe('Renderer - button', () => {
  it('render a button', () => {
    const element = button({
      name: 'name',
      id: 'id',
      label: 'Wololo'
    })

    expect(element.textContent).equal('Wololo')
    expect(element.nodeName).equal('BUTTON')
    expect(testUtils.getAllAttributes(element)).deep.equal({
      name: 'name',
      class: 'pill-button',
      type: 'button',
      id: 'id'
    })
  })

  it('render a button with type', () => {
    const element = button({
      name: 'name',
      id: 'id',
      label: 'Wololo',
      className: 'lala',
      type: 'submit'
    })

    expect(element.textContent).equal('Wololo')
    expect(element.nodeName).equal('BUTTON')
    expect(testUtils.getAllAttributes(element)).deep.equal({
      name: 'name',
      class: 'lala',
      id: 'id',
      type: 'submit'
    })
  })
})
