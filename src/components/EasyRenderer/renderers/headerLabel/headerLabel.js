const headerLabel = ({ label, content, level = 2 }) => {
  const wrapElement = document.createElement('div')
  const headerElement = document.createElement(`h${level}`)
  const contentElement = document.createElement('p')

  wrapElement.className = 'header-label'
  headerElement.className = `header-label__header header-label__header--level-${level}`
  contentElement.className = 'header-label__description'

  headerElement.textContent = label
  contentElement.textContent = content

  wrapElement.appendChild(headerElement)
  wrapElement.appendChild(contentElement)

  return wrapElement
}

export default headerLabel
