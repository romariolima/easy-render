import { expect } from 'chai'
import sinon from 'sinon'
import map from './map'

describe('Renderer - map', () => {
  const GOOGLE_MAP_EMBED_URI = 'https://www.google.com/maps/embed/v1/place?key=AIzaSyD4l6TNXdMKiwNhU7jEjK1ulkNnUnbHosE'
  let bindElement

  beforeEach('create bindElement', () => {
    bindElement = document.createElement('input')
    bindElement.id = 'bind'
    bindElement.value = 'Sample Address'

    document.body.appendChild(bindElement)
  })

  afterEach('remove bindElement', () => {
    bindElement.remove()
  })

  it('render a map component', () => {
    const mapElement = map({ id: 'id', bindTo: '#bind' })

    expect(mapElement.nodeName).equal('FIGURE')
    expect(mapElement.className).equal('map')
    expect(mapElement.querySelector('.map__iframe').id).equal('id')
    expect(mapElement.querySelector('.map__iframe').nodeName).equal('IFRAME')
    expect(mapElement.querySelector('.map__iframe').src)
    .equal(`${GOOGLE_MAP_EMBED_URI}&q=${encodeURIComponent('Sample Address')}`)
  })

  it('change iframe src when bound element changes value', () => {
    const clock = sinon.useFakeTimers()
    const mapElement = map({ id: 'id', bindTo: '#bind' })

    bindElement.value = 'Rua Epaminondas'
    bindElement.dispatchEvent(new window.Event('blur'))

    clock.tick(10)

    expect(mapElement.querySelector('.map__iframe').src)
    .equal(`${GOOGLE_MAP_EMBED_URI}&q=${encodeURIComponent('Rua Epaminondas')}`)

    bindElement.value = ''
    bindElement.dispatchEvent(new window.Event('blur'))

    clock.tick(10)

    expect(mapElement.querySelector('.map__iframe').src)
    .equal(`${GOOGLE_MAP_EMBED_URI}&q=%20`)

    bindElement.value = 'Rua Epaminondas, 200'
    bindElement.dispatchEvent(new window.Event('change'))

    clock.tick(10)

    expect(mapElement.querySelector('.map__iframe').src)
    .equal(`${GOOGLE_MAP_EMBED_URI}&q=${encodeURIComponent('Rua Epaminondas, 200')}`)

    bindElement.value = ''
    bindElement.dispatchEvent(new window.Event('change'))

    clock.tick(10)

    expect(mapElement.querySelector('.map__iframe').src)
    .equal(`${GOOGLE_MAP_EMBED_URI}&q=%20`)

    clock.restore()
  })
})
