const GOOGLE_MAP_EMBED_URI = 'https://www.google.com/maps/embed/v1/place?key=AIzaSyD4l6TNXdMKiwNhU7jEjK1ulkNnUnbHosE'

const setAddressUrl = (address) => {
  let q = '%20'
  if (address) {
    q = encodeURIComponent(address)
  }
  return `${GOOGLE_MAP_EMBED_URI}&q=${q}`
}

const createWrap = () => {
  const wrapElement = document.createElement('figure')
  wrapElement.className = 'map'

  return wrapElement
}

const createIframe = ({ id }) => {
  const iframeElement = document.createElement('iframe')
  iframeElement.className = 'map__iframe'
  iframeElement.id = id
  iframeElement.setAttribute('allowfullscreen', '')

  return iframeElement
}

const setEventListener = ({ boundElement, iframeElement }) => {
  /* eslint-disable no-param-reassign */
  const update = () => setTimeout(() => {
    if (iframeElement.src !== setAddressUrl(boundElement.value)) {
      iframeElement.src = setAddressUrl(boundElement.value)
    }
  }, 10)

  boundElement.addEventListener('blur', () => update(), false)
  boundElement.addEventListener('change', () => update(), false)
}

const map = ({ id, bindTo }) => {
  const boundElement = document.querySelector(bindTo)
  const wrapElement = createWrap()
  const iframeElement = createIframe({ id, boundElement })

  iframeElement.src = setAddressUrl(boundElement.value)

  setEventListener({ boundElement, iframeElement })

  wrapElement.appendChild(iframeElement)

  return wrapElement
}

export default map
