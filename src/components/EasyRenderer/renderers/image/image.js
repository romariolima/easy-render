const image = ({ id, src = '' }) => {
  const wrapElement = document.createElement('div')
  const innerWrapElement = document.createElement('div')
  const imageElement = document.createElement('img')

  wrapElement.className = 'circled-image'
  innerWrapElement.className = 'circled-image__wrapper'

  wrapElement.id = id
  imageElement.src = src

  imageElement.className = 'circled-image__image'

  innerWrapElement.appendChild(imageElement)
  wrapElement.appendChild(innerWrapElement)

  return wrapElement
}

export default image
