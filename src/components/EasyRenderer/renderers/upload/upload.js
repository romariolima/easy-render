import button from '../button'

const previewFile = bindTo => (e) => {
  const uploadFile = e.target.files[0]
  const reader = new FileReader()

  reader.addEventListener('load', () => {
    document.querySelector(bindTo).querySelector('img').src = reader.result
  }, false)

  if (uploadFile) {
    reader.readAsDataURL(uploadFile)
  }
}

const createUploadElement = ({ bindTo, id, name }) => {
  const uploadElement = document.createElement('input')
  uploadElement.className = 'upload__file'
  uploadElement.id = id
  uploadElement.name = name
  uploadElement.setAttribute('type', 'file')
  uploadElement.setAttribute('accept', 'image/*')
  uploadElement.addEventListener('change', previewFile(bindTo), false)

  return uploadElement
}

const upload = ({ label, name, id, bindTo }) => {
  const wrapElement = document.createElement('div')
  const uploadElement = createUploadElement({ bindTo, id, name })
  const buttonElement = button({
    label,
    className: 'pill-button-hollow upload__button'
  })

  wrapElement.className = 'upload'

  buttonElement.addEventListener('click', (e) => {
    e.preventDefault()
    uploadElement.click()
  }, false)

  wrapElement.appendChild(uploadElement)
  wrapElement.appendChild(buttonElement)

  return wrapElement
}

export default upload
