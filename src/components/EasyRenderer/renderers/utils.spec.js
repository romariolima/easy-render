import sinon from 'sinon'
import utils from './utils'

describe('Renderers - utils', () => {
  it('#setAttributes()', () => {
    const setAttribute = sinon.spy()
    const element = { setAttribute }
    const props = {
      foo: 'bar',
      cleyton: 'llama'
    }

    utils.setAttributes(element, props)

    sinon.assert.callCount(setAttribute, 2)
    sinon.assert.calledWith(element.setAttribute, 'foo', 'bar')
    sinon.assert.calledWith(element.setAttribute, 'cleyton', 'llama')
  })
})
