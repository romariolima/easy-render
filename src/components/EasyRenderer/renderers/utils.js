const setAttributes = (element, props) =>
  Object.keys(props).forEach(key => props[key] && element.setAttribute(key, props[key]))

export default { setAttributes }
