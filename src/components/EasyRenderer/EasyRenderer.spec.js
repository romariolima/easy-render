import sinon from 'sinon'
import { expect } from 'chai'
import EasyRenderer from './EasyRenderer'

describe('EasyRenderer', () => {
  const fields = [{
    name: 'txtFullname',
    id: 'txtFullname',
    label: 'Nome Completo',
    value: '',
    type: 'text',
    component: 'input'
  }, {
    name: 'txtCpf',
    id: 'txtCpf',
    label: 'CPF',
    value: '',
    type: 'text',
    component: 'input'
  }]
  let target

  beforeEach('add target element', () => {
    target = document.createElement('div')

    document.body.appendChild(target)
  })

  afterEach('remove target element', () => {
    target.remove()
  })

  it('render form with fields in target element', () => {
    const stub = sinon.stub(EasyRenderer.RENDERER_STRATEGIES, 'input')

    const firstElement = document.createElement('input')
    firstElement.id = 'element-1'
    const secondElement = document.createElement('input')
    secondElement.id = 'element-2'

    stub.withArgs(fields[0]).returns(firstElement)
    stub.withArgs(fields[1]).returns(secondElement)

    new EasyRenderer({
      fields,
      target,
      onFormSubmit: () => {}
    })

    sinon.assert.calledTwice(stub)
    sinon.assert.calledWith(stub, fields[0])
    sinon.assert.calledWith(stub, fields[1])

    expect(target.children[0].nodeName).equal('FORM')

    expect(target.querySelector('#element-1')).not.equal(null)
    expect(target.querySelector('#element-2')).not.equal(null)

    EasyRenderer.RENDERER_STRATEGIES.input.restore()
  })
})
