import webpack from 'webpack'
import path from 'path'
import HtmlWebpackPlugin from 'html-webpack-plugin'

export default {
  context: path.resolve(__dirname, '.'),
  devtool: 'source-map',
  entry: [
    'webpack-dev-server/client?http://localhost:4242',
    'webpack/hot/only-dev-server',
    './src/app/easy-render.js'
  ],
  resolve: {
    root: path.resolve(__dirname, '.'),
    extensions: ['', '.js', '.css']
  },
  devServer: {
    contentBase: './dist/',
    noInfo: true,
    hot: true,
    inline: true,
    historyApiFallback: true,
    port: 4242,
    host: 'localhost'
  },
  module: {
    loaders: [{
      test: /\.js$/,
      loader: 'babel',
      exclude: /(node_modules)/
    }, {
      test: /\.json$/,
      loader: 'json'
    }, {
      test: /\.css$/,
      loaders: [
        'style',
        'css?sourceMap&importLoaders=1',
        'postcss?sourceMap'
      ]
    }]
  },
  plugins: [
    new webpack.NoErrorsPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({
      template: './src/app/index.html'
    })
  ]
}
