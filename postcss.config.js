const postcssImport = require('postcss-import')
const webpack = require('webpack')

module.exports = {
  plugins: [
    postcssImport({ addDependencyTo: webpack }),
    require('precss')(),
    require('postcss-font-magician')()
  ]
}
