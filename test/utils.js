const getAllAttributes = el =>
  Array.from(el.attributes).reduce((obj, attr) =>
    Object.assign(obj, {
      [attr.nodeName]: attr.nodeValue
    }), {})

export default { getAllAttributes }
